const Form = {
	fields: {
		firstName: {
			label: 'Given name',
			inputType: 'text'
		},
		lastName: {
			label: 'Surname',
			inputType: 'text'
		},
		email: {
			label: 'Email',
			inputType: 'email'
		},
		phone: {
			label: 'Phone',
			inputType: 'tel'
		},
		streetNumber: {
			label: 'House name or #',
			inputType: 'text'
		},
		streetName: {
			label: 'Street',
			inputType: 'text'
		},
		suburb: {
			label: 'Suburb',
			inputType: 'text'
		},
		state: {
			label: 'State',
			inputType: 'text'
		},
		postcode: {
			label: 'Postcode',
			inputType: 'text'
		},
		country: {
			label: 'Country',
			inputType: 'text'
		}
	},
	sections: {
		personalDetails: {
			label: 'Personal Details',
			fields:['firstName','lastName','email','phone']
		},
		'addressDetails': {
			label: 'Address',
			fields: ['streetNumber','streetName','suburb','state','postcode','country']
		}
	}
}

export default Form;