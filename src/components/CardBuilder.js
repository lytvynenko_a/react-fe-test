import React, { Component } from 'react'
import CardBuilderForm from './CardBuilderForm'
import CardBuilderPreview from './CardBuilderPreview'
import Form from '../modules/Form';

class CardBuilder extends Component {

	constructor(props) {
		super(props)
		this.state = {};
	}

	componentDidMount() {
		Object.keys(Form.fields).map((field) => this.setState({
			[field]: ''
		}))
	}

	onChangeHandler(fieldId, evt) {
		this.setState({
			[fieldId]: evt.target.value
		})
	}

	onImageSelect(e) {
		e.preventDefault()
		var isIE = (navigator.appName == 'Microsoft Internet Explorer');
		var path = e.target.value;
		if (isIE) {
			this.setState({
				imgSrc: path
			})
		} else {
			let reader = new FileReader()
			let file = e.target.files[0]
			reader.readAsDataURL(file)

			reader.onloadend = () => {
				this.setState({
					imgSrc: reader.result
				})
			}
		}
	}

	render() {
		return ( < div id = "app" >
			< div className = "layout-pane" >
			< div className = "pane-inner" >
			< CardBuilderForm form = {
				Form
			}
			onChangeHandler = {
				this.onChangeHandler.bind(this)
			}
			onImageSelect = {
				this.onImageSelect.bind(this)
			}
			/> < /div> < /div> < div className = "layout-pane" >
			< div className = "pane-inner" >
			< CardBuilderPreview form = {
				this.state
			}
			imgSrc = {
				this.state.imgSrc
			}
			/> < /div> < /div> < /div>
		)
	}
}

export default CardBuilder