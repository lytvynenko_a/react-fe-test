import React, {Component, PropTypes} from 'react'
require('styles/Card.less');


const headShot = require('../images/head.png');

class CardFormPreview extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		let form = this.props.form;
		let imgSrc = this.props.imgSrc ? this.props.imgSrc : headShot;
		return (
						<div id="preview">
							<h2>HCARD PREVIEW</h2>
							<div className="preview-inner">
								<div className="preview-header">
									<div className="preview-fullname">
										<span>
											{form.firstName}&nbsp;
										</span>
										<span>
											{form.lastName}
										</span>
									</div>
									<div className="preview-image">
										<img id="preview-image" src={imgSrc}/>
									</div>
								</div>
								<div className="preview-body">
									<div className="preview-field full-width">
										<div className="preview-field-label">
											Email
										</div>
										<div className="preview-field-value">
											{form.email}
										</div>
									</div>
									<div className="preview-field full-width">
										<div className="preview-field-label">
											Phone
										</div>
										<div className="preview-field-value">
											{form.phone}
										</div>
									</div>
									<div className="preview-field full-width">
										<div className="preview-field-label">
											Address
										</div>
										<div className="preview-field-value">
											<span>
												{form.streetNumber}
											</span>
											<span>
												{form.streetName}
											</span>
										</div>
									</div>
									<div className="preview-field full-width">
										<div className="preview-field-label">
											
										</div>
										<div className="preview-field-value">
											<span>
												{form.suburb}
											</span>
											{form.suburb && form.state ? ', ' : ''}
											<span>
												{form.state}
											</span>
										</div>
									</div>
									<div className="preview-field half-width">
										<div className="preview-field-label">
											Postcode
										</div>
										<div className="preview-field-value">
											{form.postcode}
										</div>
									</div>
									<div className="preview-field half-width">
										<div className="preview-field-label">
											Country
										</div>
										<div className="preview-field-value">
											{form.country}
										</div>
									</div>
								</div>
							</div>
						</div>)
	}
}

CardFormPreview.propTypes = {
	form:PropTypes.object,
	imgSrc:PropTypes.string
}

export default CardFormPreview