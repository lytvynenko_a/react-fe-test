import React, {Component, PropTypes} from 'react'
require('styles/Form.less');

class CardFormBuilder extends Component {
	constructor(props) {
		super(props)
	}

	renderField(fieldId, idx) {
		const field = this.props.form.fields[fieldId];
		const className = 'input-wrapper ' + (idx % 2 === 0 ? 'odd' : 'even');
		return (
			<div key={fieldId} className={className}>
				<label for={fieldId}>{field.label}</label>
				<input name={fieldId} type={field.inputType} onChange={this.props.onChangeHandler.bind(this, fieldId)}/>
			</div>
		)
	}

	renderSection(section) {
		return (
			<div className="form-section">
				<h2>{section.label}</h2>
				{ section.fields.map((fieldId, idx)=>this.renderField(fieldId, idx)) }
			</div>
			)
	}


	render() {
		const sections = this.props.form.sections;
		return (
			<div id="form">
				<h1>hCard Builder</h1>
				<span id="inputs-wrapper">
				{ Object.keys(sections).map((sectionId) => this.renderSection(sections[sectionId])) }
				</span>
				<div className="input-wrapper odd">
					<button className="btn btn-upload">
						Upload Avatar
						<input type="file" ref="file" onChange={this.props.onImageSelect.bind(this)} accept="image/*" />
					</button>
				</div>
				<div className="input-wrapper even">
					<button className="btn btn-submit">
						Create hCard
					</button>
				</div>
			</div>)
	}
}


CardFormBuilder.propTypes = {
	form:PropTypes.object.isRequired,
	onChangeHandler:PropTypes.func.isRequired,
	onImageSelect:PropTypes.func.isRequired
}

export default CardFormBuilder