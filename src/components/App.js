require('normalize.css/normalize.css');
require('styles/App.less');

import React from 'react';
import CardBuilder from './CardBuilder';

class App extends React.Component {
  render() {
    return (
      <div className="full-screen">
			<CardBuilder />
      </div>
    );
  }
}

App.defaultProps = {
};

export default App;
