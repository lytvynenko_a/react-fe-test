# Domain Front End Test (React.js)
## Prerequisites
- Node 5
- NPM 3

Clone repository to target folder and run ```npm install```.

To run dev server use ``npm run serve``.

To build dist package use ``npm run dist`` - the bundle will be generated in ``dist`` folder.

To run Linter use ``npm run lint``


The source code is located in ``src`` folder.